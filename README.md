
# Rusty Bell

Rusty Bell is a simple application to start server via the Wake-on-LAN feature. 

## Configuration

There will be a single file, storing all the server host names and the corresponding MACs.
The file has to be named ```hosts```, without any extension to work.
This is a constant in the application.

If you want to add another computer to the application, add the host to the file and fill in the MAC address.
The host name and the mac has to be separated by a SPACE, the MAC address has to be separated by a colon.

### Example Configuration

```
adam 01:02:03:ab:cd:ef
eva 98:76:54:32:10:ff
```

## Usage

Call the application with the intended host name of the server you want to start.
If the host is not in the config file, the application panics and returns an error message.
Otherwise, if the host is valid, a magic packet will be prepared and the host should start.

If the host does not start, then try checking the MAC address.
