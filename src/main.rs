use std::fs::File;
use std::io::{BufReader, BufRead};
use std::error::Error;
use std::process::exit;

extern crate hex;
extern crate pnet;

use pnet::datalink::{self, NetworkInterface};
use std::net::UdpSocket;

const DELIMITER: char = ':';
const HOST_FILE: &str = "/rustybell/hosts";
const SOCKET_TEMPLATE: &str = "0.0.0.0:0";
const USAGE: &str = "rustybell [host]\n
Example:
rustybell adam
rustybell eva

The host has to be configured in the 'hosts' file in the following format:
[hostname] [MAC]

Example:
adam aa:bb:cc:dd:ee:ff

The host file has to be in the path $HOME/.config/rustybell/hosts
";


///
/// Config struct to store the host.
///
struct Config {
    host: String,
}

impl Config {
    fn new(host: String) -> Self  {
        Config {
            host
        }
    }
}

///
/// Print the usage text of the application.
///
fn usage() {
    println!("{}", USAGE);
}

///
/// Parses the hosts of the HOST_FILE to a hash map
///
fn parse_host(s: &str) -> Result<(&str, [u8; 6]), Box<dyn Error>> {
    let s: Vec<&str> = s.split_whitespace().collect();
    if s.len() != 2 {
        panic!("Failed parsing host");
    }

    let host = s[0];
    let mac = s[1];
    let mac = decode_mac(mac).unwrap_or_else(|err|
        panic!(format!("Failed parsing host: {}", err.to_string()))
    );

    Ok((host, mac))
}

///
/// Decodes the string representation of a MAC address
/// to the byte value of it.
///
fn decode_mac(s: &str) -> Result<[u8; 6], Box<dyn Error>> {
    let mut mac: [u8; 6] = [0x0; 6];
    let bytes = s.replace(DELIMITER, "");
    let bytes = hex::decode(bytes).expect("Failed decoding MAC address");

    for i in 0..mac.len() {
        let p = match bytes.get(i) {
            Some(v) => v.to_owned(),
            None => panic!("Failed decoding MAC address"),
        };
        mac[i] = p;
    }

    Ok(mac)
}

///
/// Returns the magic packet which will be flushed through
/// the network to start the server remotely
///
fn generate_magic_packet(mac: [u8; 6]) -> [u8; 102] {
    // first 6 bytes = 0xff
    let mut magic_packet: [u8; 102] = [0xff; 102];

    // overwrite with MAC repeatingly
    // 16x MAC
    for i in 0..16 {
        // each byte of MAC
        for j in 0..mac.len() {
            // calc the position for byte of MAC
            let pos = 6 + i * mac.len() + j;
            magic_packet[pos] = mac[j];
        }
    }

    magic_packet
}

///
/// Flushes the network with the magic packet
///
///
fn flush_network_with(magic: [u8; 102]) -> Result<(), Box<dyn Error>> {
    // https://i.imgflip.com/38xlbk.jpSome($XDG_CONFIG_HOME) or Some($HOME/.config)g

    // choose interface
    let mut ifaces: Vec<NetworkInterface> = Vec::new();
    for iface in datalink::interfaces() {
        if iface.is_up() && !iface.is_loopback() {
            ifaces.push(iface);
        }
    }

    if ifaces.is_empty() {
        panic!("Failed determining a proper interface");
    }


    for iface in ifaces {
        match iface.ips.first() {
            Some(ip) => {
                let ip = ip.broadcast();
                let target_host = format!("{}:{}", ip.to_string(), 9);
                let socket = UdpSocket::bind(SOCKET_TEMPLATE).unwrap();
                socket.set_broadcast(true)?;
                socket.send_to(&magic, target_host)?;
            },
            None => (),
        }
    }

    Ok(())
}

fn get_host_file_path() -> Result<String, Box<dyn Error>> {
    let config_dir = dirs::config_dir().expect("Failed to determine config directory");
    let config_dir = config_dir.to_str().expect("Failed to parse config directory");
    Ok(format!("{}{}",config_dir, HOST_FILE))
}

///
/// Executes the actual program and returns any errors
/// that might occur.
///
fn run(cfg: &Config) -> Result<(), Box<dyn Error>> {
    let file = File::open(get_host_file_path()?)?;
    let lines = BufReader::new(file)
        .lines()
        .collect::<Result<Vec<_>, _>>()?;

    let mac: [u8; 6] = lines
        .iter()
        .filter_map(|l| parse_host(l).ok())
        .find(|(h, _)| *h == &cfg.host)
        .ok_or_else(|| format!("Host {} is not configured", cfg.host))?
        .1;

    let mp: [u8; 102] = generate_magic_packet(mac);
    flush_network_with(mp)?;

    Ok(())
}

///
/// Parses the arguments to a config instance
///
fn parse_args() -> Result<Config, Box<dyn Error>> {
    // parse host argument
    let host = std::env::args().nth(1).expect("Missing host argument");

    Ok(Config::new(host))
}

fn main() {
    // parse arguments to config
    let cfg = parse_args().unwrap_or_else(|err| {
        eprintln!("Failed parsing arguments: {}.", err.to_string());
        usage();
        exit(1);
    });

    run(&cfg).unwrap_or_else(|err| {
        eprintln!("Application error: {}.", err.to_string());
        usage();
        exit(1);
    });
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_host() {
        assert!(false);
    }

    #[test]
    fn test_decode_mac() {
        let decode: [u8; 6] = [0x00, 0x00, 0x00, 0x00, 0x01, 0xff];
        let encode = String::from("00:00:00:00:01:ff");

        assert_eq!(decode, decode_mac(encode).unwrap());
    }

    #[test]
    fn test_generate_magic_packet() {
        assert!(false);
    }
}
